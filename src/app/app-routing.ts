import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {CommonModule} from '@angular/common';
import {HomeComponent} from './home/home.component';

const routes: Routes = [
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent, data: {title: 'Главная'}},
  {path: 'films', loadChildren: './films/films.module#FilmsModule', data: {title: 'Фильмы'}},
  {path: 'search', loadChildren: './search/search.module#SearchModule', data: {title: 'Поиск'}},
  {path: '**', redirectTo: 'home'}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(routes, {useHash: true})
  ],
  exports: [RouterModule]
})

export class AppRouting {
}
