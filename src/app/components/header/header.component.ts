import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  text: string = '';

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  search() {
    const text: string = this.text.trim();
    if (text !== '') {
      this.router.navigate(['/search'], {queryParams: {t: text}});
    }
  }
}
