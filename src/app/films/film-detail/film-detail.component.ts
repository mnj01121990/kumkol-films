import {Component, OnInit} from '@angular/core';
import {Film, FilmsService} from '../../services/films.service';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'app-film-detail',
  templateUrl: './film-detail.component.html',
  styleUrls: ['./film-detail.component.css']
})
export class FilmDetailComponent implements OnInit {
  subscription;
  film: Film;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private filmsService: FilmsService
  ) {
  }

  ngOnInit() {
    this.subscription = this.router.events.subscribe(e => {
      if (e instanceof NavigationEnd) {
        this.getFilmInfo();
      }
    });
    this.getFilmInfo();
  }

  getFilmInfo() {
    const id: string = this.route.snapshot.params['id'];
    this.filmsService.getFilmById(id).subscribe((film: Film) => {
      if (film.Response === 'True') {
        this.film = film;
      } else {
        this.back();
      }
    }, error => {
      this.back();
    });
  }

  back() {
    this.router.navigate(['/films']);
  }
}
