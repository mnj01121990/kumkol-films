import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FilmsComponent} from './films.component';
import {FilmDetailComponent} from './film-detail/film-detail.component';
import {FilmsRouting} from './films.routing';
import {FilmsService} from '../services/films.service';
import {FilmShortDetailModule} from '../shared/modules/film-short-detail/film-short-detail.module';

@NgModule({
  imports: [
    CommonModule,
    FilmShortDetailModule,
    FilmsRouting
  ],
  declarations: [FilmsComponent, FilmDetailComponent],
  providers: [FilmsService]
})
export class FilmsModule {
}
