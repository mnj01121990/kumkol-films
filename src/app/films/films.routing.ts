import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {FilmsComponent} from './films.component';
import {FilmDetailComponent} from './film-detail/film-detail.component';

const routes: Routes = [
  {
    path: '',
    component: FilmsComponent
  },
  {
    path: ':id',
    component: FilmDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FilmsRouting {

}
