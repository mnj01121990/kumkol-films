import {Component, OnInit} from '@angular/core';
import {Film, FilmHelper, FilmsService} from '../services/films.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-films',
  templateUrl: './films.component.html',
  styleUrls: ['./films.component.css']
})
export class FilmsComponent implements OnInit {
  films: Film[] = [];
  total: number = 0;

  constructor(
    private filmsService: FilmsService,
    private toastr: ToastrService
  ) {
  }

  ngOnInit() {
    this.filmsService.searchFilms('abc').subscribe((data: FilmHelper) => {
      if (data.Response === 'True') {
        this.films = data.Search;
        this.total = parseInt(data.totalResults, 10);
      } else {
        this.toastr.error(data.Error);
      }
    });
  }
}
