import {Component, Input, OnInit} from '@angular/core';
import {Film} from '../../../services/films.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-film-short-detail',
  templateUrl: './film-short-detail.component.html',
  styleUrls: ['./film-short-detail.component.css']
})
export class FilmShortDetailComponent implements OnInit {
  @Input() film: Film;

  constructor(private router: Router) {
  }

  ngOnInit() {
  }

  navigateToDetail() {
    this.router.navigate(['/films', this.film.imdbID]);
  }
}
