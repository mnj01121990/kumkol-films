import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FilmShortDetailComponent} from './film-short-detail.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [FilmShortDetailComponent],
  exports: [FilmShortDetailComponent]
})
export class FilmShortDetailModule {
}
