import {Component, OnInit} from '@angular/core';
import {Film, FilmHelper, FilmsService} from '../services/films.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  news: Film[] = [];

  constructor(
    private filmsService: FilmsService,
    private toastr: ToastrService
  ) {
  }

  ngOnInit() {
    this.filmsService.getNewFilms('abc').subscribe((data: FilmHelper) => {
      if (data.Response === 'True') {
        this.news = data.Search;
      } else {
        this.toastr.error(data.Error);
      }
    });
  }
}
