import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, NavigationEnd, Router} from '@angular/router';
import {Film, FilmHelper, FilmsService} from '../services/films.service';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  subscription;
  text: string = '';
  films: Film[] = [];
  total: number = 0;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private filmsService: FilmsService,
    private toastr: ToastrService
  ) {
  }

  ngOnInit() {
    this.subscription = this.router.events.subscribe(e => {
      if (e instanceof NavigationEnd) {
        this.search();
      }
    });
    this.search();
  }

  search() {
    const text: string = this.route.snapshot.queryParams['t'];
    this.text = text.trim();
    if (this.text === '') {
      this.toastr.error('Текст поиска пуст');
    } else {
      this.filmsService.searchFilms(this.text).subscribe((data: FilmHelper) => {
        if (data.Response === 'True') {
          this.films = data.Search;
          this.total = parseInt(data.totalResults, 10);
        } else {
          this.toastr.error(data.Error);
        }
      });
    }
  }
}
