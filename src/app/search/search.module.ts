import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SearchComponent} from './search.component';
import {FilmsService} from '../services/films.service';
import {SearchRouting} from './search.routing';
import {FilmShortDetailModule} from '../shared/modules/film-short-detail/film-short-detail.module';

@NgModule({
  imports: [
    CommonModule,
    FilmShortDetailModule,
    SearchRouting
  ],
  declarations: [SearchComponent],
  providers: [FilmsService]
})
export class SearchModule {
}
