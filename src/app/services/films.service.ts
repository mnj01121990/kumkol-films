import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';

export interface Rating {
  Source: string;
  Value: string;
}

export interface Film {
  Actors: string;
  Awards: string;
  BoxOffice: string;
  Country: string;
  Director: string;
  Genre: string;
  Language: string;
  Metascore: string;
  Plot: string;
  Production: string;
  Poster: string;
  Rated: string;
  Ratings: Rating[];
  Released: string;
  Response: string;
  Runtime: string;
  Title: string;
  Type: string;
  Website: string;
  Writer: string;
  Year: string;
  imdbID: string;
  imdbRating: string;
  imdbVotes: string;
  totalSeasons: string;
}

export interface FilmHelper {
  Response: string;
  Search: Film[];
  totalResults: string;
  Error: string;
}

@Injectable({
  providedIn: 'root'
})
export class FilmsService {

  constructor(private http: HttpClient) {
  }

  getNewFilms(text: string) {
    const url = `${environment.url}&s=${text}`;
    return this.http.get(url);
  }

  searchFilms(text: string) {
    const url = `${environment.url}&s=${text}`;
    return this.http.get(url);
  }

  getFilmById(id: string) {
    const url = `${environment.url}&i=${id}`;
    return this.http.get(url);
  }
}
